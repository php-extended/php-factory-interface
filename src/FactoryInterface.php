<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-factory-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Factory;

use Iterator;
use Stringable;

/**
 * FactoryInterface interface file.
 * 
 * This interface represents a single factory capability. A factory is an object
 * that can create specific objects out of non parseable resources (like
 * randomness sources).
 * 
 * @author Anastaszor
 * @template T of object
 */
interface FactoryInterface extends Stringable
{
	
	/**
	 * Creates a new object of the type this factory is able to create.
	 * 
	 * @return T
	 */
	public function create() : object;
	
	/**
	 * Creates multiple objects from this factory. Negative quantity is
	 * interpreted as zero and an empty array is returned.
	 * 
	 * @param integer $quantity
	 * @return array<T>
	 */
	public function createArray(int $quantity = 1) : array;
	
	/**
	 * Creates an iterator that gives objects that are created from a factory.
	 * If the given max quantity is negative, an limitless iterator is returned.
	 * 
	 * @param integer $maxQuantity
	 * @return Iterator<T>
	 */
	public function createIterator(int $maxQuantity = -1) : Iterator;
	
}
