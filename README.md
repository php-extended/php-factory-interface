# php-extended/php-factory-interface

A library to generate objects from non parseable sources. It represents abstractions for parsers that may be able to build any object.

![coverage](https://gitlab.com/php-extended/php-factory-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-factory-interface ^8`


## Basic Usage

This library is an interface-only library.

This library has no direct implementations, but many other interface libraries depends on it.


## License

MIT (See [license file](LICENSE)).
